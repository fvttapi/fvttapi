
import signal
import os
from pathlib import Path
from time import sleep

from dirsync import sync
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers.polling import PollingObserver

from fvttapi.settings import MEDIA_ROOT
from fvttapi.settings.app import FOUNDRY_HOME


WATCH_DIR = FOUNDRY_HOME / 'Data' / 'worlds' / 'heavy-rain' / 'data'

def on_created(event):
    print(event)
    return
    #logger.info(f"CREATED: {event.src_path}")
    size = -1
    while size != os.path.getsize(event.src_path):
        size = os.path.getsize(event.src_path)
        sleep(1)
    #media = MediaFile(Path(event.src_path))
    #media.process_file()


def get_observer():
    observer = Observer()
    #if config.windows and IS_DOCKER:
    #    logger.info('WARNING: Using less performant observer because host is Windows.')
    #    observer = PollingObserver()
    return observer


def schedule_observer(observer):
    fs_handle = PatternMatchingEventHandler(
        patterns=['*'],
        #ignore_patterns=get_exclude_dirs(),
        ignore_directories=False,
        case_sensitive=True
    )
    fs_handle.on_created = on_created

    watch_dir = WATCH_DIR

    print(f'Watching directory {watch_dir.resolve()}')
    observer.schedule(fs_handle, f'{watch_dir.resolve()}', recursive=True)
    return observer


observer = get_observer()


def finish(signum, frame):
    print('Exiting application...')
    try:
        observer.stop()
        observer.join()
    except:
        print('Error stopping observer.')
    exit(0)


signal.signal(signal.SIGTERM, finish)
signal.signal(signal.SIGINT, finish)

DEST_ROOT: Path = MEDIA_ROOT / 'sync' / 'worlds'
DEST_DIR = DEST_ROOT / 'heavy-rain' / 'data'
DEST_DIR.mkdir(parents=True, exist_ok=True)


sync(WATCH_DIR, DEST_DIR, 'sync')

print('Starting watcher')
observer = schedule_observer(observer)
observer.start()

while True:
    sleep(1)

