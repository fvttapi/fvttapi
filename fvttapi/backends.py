import binascii
import json
from pathlib import Path
import hashlib

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend

from fvttapi.settings.app import FOUNDRY_HOME
from fvttapi.utils import get_default_world


User = get_user_model()


class FoundryBackend(BaseBackend):
    user_db = FOUNDRY_HOME / 'Data' / 'worlds' / get_default_world() / 'data' / 'users.db'
    # user_db = Path('C:\\Users\\cclloyd\\AppData\\Local\\FoundryVTT\\Data\\worlds\\test\\data\\users.db')

    def authenticate(self, request, username=None, password=None):
        with open(self.user_db, 'r', encoding='utf-8') as file:
            data = file.read().split('\n')
            players = [json.loads(n) for n in data if len(n) > 0]
            file.close()
        for player in players:
            if player['name'] == username:
                salt_str = player['passwordSalt']

                hex = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt_str.encode('utf-8'), 1000, 64)
                hash_str = binascii.hexlify(hex).decode("utf-8")
                if hash_str == player['password']:
                    try:
                        user = User.objects.get(username=username)
                        user.is_staff = True
                        user.is_superuser = player['role'] == 4
                        user.save()
                    except User.DoesNotExist:
                        user = User(username=username)
                        user.is_staff = True
                        user.is_superuser = player['role'] == 4
                        user.save()
                        user = user
                    return user
                else:
                    return None
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None