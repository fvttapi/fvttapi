"""fvttapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.views import generic
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from .apps.world.urls import url_patterns as world_urls
from django.urls import re_path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from fvttapi.apps.auth.urls import url_patterns as auth_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', generic.RedirectView.as_view(url='/api/', permanent=False)),
    re_path(r'^api/$', get_schema_view(), name='openapi-schema'),

    re_path(r'^api-auth/', include('rest_framework.urls')),
    re_path(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework_auth')),
    re_path(r'^api/auth/token/obtain/$', TokenObtainPairView.as_view()),
    re_path(r'^api/auth/token/refresh/$', TokenRefreshView.as_view()),

    *auth_urls,

    *world_urls,
]
