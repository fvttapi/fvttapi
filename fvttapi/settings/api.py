from datetime import timedelta

from . import env, BASE_DIR

AUTH_CLASS = env.str('AUTH_CLASS', 'rest_framework.permissions.IsAuthenticated')

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        AUTH_CLASS,
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100,
}

SIMPLE_JWT = {
    'REFRESH_TOKEN_LIFETIME': timedelta(days=30),
}
