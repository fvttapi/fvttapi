import json

from . import env

FOUNDRY_HOME = env.path('FOUNDRY_HOME', '/data')
FOUNDRY_HOSTNAME = env.str('FOUNDRY_HOSTNAME', None)

_FOUNDRY_CONFIG = dict()
_config_file = FOUNDRY_HOME / 'Config' / 'options.json'
if _config_file.exists():
    with open(_config_file, 'r', encoding='utf-8') as file:
        _FOUNDRY_CONFIG = json.loads(file.read())
        file.close()
FOUNDRY_CONFIG = _FOUNDRY_CONFIG

