from . import env


# Application definition

INSTALLED_APPS = [
    'rest_framework',
    'fvttapi.apps.auth',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
