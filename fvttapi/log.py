from logging import Filter
import re


class SkipStaticFilter(Filter):
    pattern = r'\[.+\]\[.+\]\s\"GET\s\/static'
    """ Logging filter to skip logging of staticfiles to terminal """
    def filter(self, record):
        m = re.match(self.pattern, record.getMessage())
        return True if m else False

