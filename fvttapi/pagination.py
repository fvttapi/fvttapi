from typing import List, Any
from collections import OrderedDict
from django.core.paginator import Paginator
from django.http.response import JsonResponse
from rest_framework.request import Request

from fvttapi.settings import REST_FRAMEWORK


class ListPaginator:
    def __init__(self, request: Request):
        # Hidden HtmlRequest properties/methods found in Request.
        self._url_scheme = request.scheme
        self._host = request.get_host()
        self._path_info = request.path_info

        self._limit = int(request.query_params.get('limit', str(REST_FRAMEWORK.get('PAGE_SIZE', 100))))
        self._offset = int(request.query_params.get('offset', '0'))

    def paginate_list(self, data: List[Any]) -> OrderedDict:
        paginator = Paginator(data, self._limit)
        page = paginator.page(int(self._offset / self._limit)+1)

        previous_url = None
        next_url = None
        if self._host and self._path_info:
            if page.has_previous():
                previous_url = '{}://{}{}?limit={}&offset={}'.format(self._url_scheme, self._host, self._path_info, self._limit, self._offset-self._limit)
            if page.has_next():
                next_url = '{}://{}{}?limit={}&offset={}'.format(self._url_scheme, self._host, self._path_info, self._limit, self._offset+self._limit)

        response_dict = OrderedDict([
            ('count', len(data)),
            ('next', next_url),
            ('previous', previous_url),
            ('results', data[self._offset:self._offset+self._limit])
        ])
        return response_dict
