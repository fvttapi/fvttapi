import json
import os
import subprocess
from pathlib import Path

import plyvel
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from fvttapi.pagination import ListPaginator
from fvttapi.settings import MEDIA_ROOT
from fvttapi.settings.app import FOUNDRY_HOME


class SceneViewSet(viewsets.ViewSet):

    @staticmethod
    def get_scene(world, scene_id, include_all=False):
        db_path = FOUNDRY_HOME / 'Data' / 'worlds' / world / 'data' / 'scenes'
        db = plyvel.DB(str(db_path), create_if_missing=False)
        raw_data = [(key, value) for key, value in db]
        db.close()

        values = [json.loads(value) for key, value in raw_data]
        keys = [key.decode('utf8') for key, value in raw_data]

        scene = None
        if scene_id in keys:
            for v in values:
                if v.get('_id') == scene_id:
                    scene = v
                    break
        else:
            for v in values:
                if v.get('name') == scene_id:
                    scene = v
                    break
        if not include_all:
            if 'walls' in scene:
                del scene['walls']
            if 'drawings' in scene:
                del scene['drawings']
        return scene

    def list(self, request, *args, world_pk=None, **kwargs):
        db_path = FOUNDRY_HOME / 'Data' / 'worlds' / world_pk / 'data' / 'scenes'
        db = plyvel.DB(str(db_path), create_if_missing=False)
        raw_data = [value for key, value in db]
        db.close()

        all_data = [json.loads(value) for value in raw_data]
        for d in all_data:
            if 'walls' in d:
                del d['walls']
            if 'drawings' in d:
                del d['drawings']
        page = ListPaginator(request).paginate_list(all_data)
        return Response(page)

    def retrieve(self, request, *args, world_pk=None, pk=None, **kwargs):
        scene = SceneViewSet.get_scene(world_pk, pk)
        if not scene:
            raise NotFound(f'Scene with that ID/name does not exist in world {world_pk}')
        return Response(scene)

    @action(methods=['get'], detail=True)
    def system(self, request, *args, world_pk=None, pk=None, **kwargs):
        scene = self.get_scene(world_pk, pk)
        return Response(scene['system'])
