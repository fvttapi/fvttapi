import json
import os

from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from fvttapi.settings.app import FOUNDRY_HOME


class FoundryViewSet(viewsets.ViewSet):

    worlds_dir = FOUNDRY_HOME / 'Data' / 'worlds'

    def list(self, request, *args, world_pk=None, **kwargs):
        worlds = [f for f in os.listdir(self.worlds_dir) if (self.worlds_dir / f).is_dir()]
        data = []
        for w in worlds:
            world_file = self.worlds_dir / w / 'world.json'
            with open(world_file, 'r', encoding='utf-8') as file:
                file_data = file.read()
                data.append(json.loads(file_data))
                file.close()
        return Response(data)

    @action(methods=['get'], detail=False)
    def config(self, request, *args, world_pk=None, pk=None, **kwargs):
        config_file = FOUNDRY_HOME / 'Config' / 'options.json'
        with open(config_file, 'r', encoding='utf-8') as file:
            config = json.loads(file.read())
            file.close()
        return_data = {
            'port': config.get('port'),
            'hostname': config.get('hostname'),
            'routePrefix': config.get('routePrefix'),
            'proxySSL': config.get('proxySSL'),
            'proxyPort': config.get('proxyPort'),
            'world': config.get('world'),
        }
        return Response(return_data)
