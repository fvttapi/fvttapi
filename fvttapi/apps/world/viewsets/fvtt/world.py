import json
import os

from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from fvttapi.settings.app import FOUNDRY_HOME


class WorldViewSet(viewsets.ViewSet):

    worlds_dir = FOUNDRY_HOME / 'Data' / 'worlds'

    def list(self, request, *args, world_pk=None, **kwargs):
        worlds = [f for f in os.listdir(self.worlds_dir) if (self.worlds_dir / f).is_dir()]
        data = []
        for w in worlds:
            world_file = self.worlds_dir / w / 'world.json'
            if world_file.exists():
                with open(world_file, 'r', encoding='utf-8') as file:
                    file_data = file.read()
                    data.append(json.loads(file_data))
                    file.close()
        return Response(data)

    def retrieve(self, request, *args, pk=None, **kwargs):
        world_file = self.worlds_dir / pk / 'world.json'
        with open(world_file, 'r', encoding='utf-8') as file:
            file_data = file.read()
            data = json.loads(file_data)
            file.close()
        return Response(data)

    @action(methods=['get'], detail=True)
    def system(self, request, *args, world_pk=None, pk=None, **kwargs):
        actor = self.get_actor(world_pk, pk)
        return Response(actor['system'])
