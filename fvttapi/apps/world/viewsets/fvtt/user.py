import json
from pathlib import Path

from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from fvttapi.settings.app import FOUNDRY_HOME


class UserViewSet(viewsets.ViewSet):

    def get_user(self, world_id, user_id=None):
        user_db = FOUNDRY_HOME / 'Data' / 'worlds' / world_id / 'data' / 'users.db'
        with open(user_db, 'r', encoding='utf-8') as file:
            data = file.read().split('\n')
            users = [json.loads(n) for n in data if len(n) > 2]
            file.close()
        if user_id:
            user = [item for item in users if item['_id'] == user_id]
            return user[0] if len(user) > 0 else None
        else:
            return users

    def list(self, request, *args, world_pk=None, **kwargs):
        return Response(self.get_user(world_pk))

    def retrieve(self, request, *args, world_pk=None, pk=None, **kwargs):
        return Response(self.get_user(world_pk, pk))

