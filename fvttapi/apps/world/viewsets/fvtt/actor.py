import json
import os
import subprocess
from pathlib import Path

import plyvel
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from fvttapi.pagination import ListPaginator
from fvttapi.settings import MEDIA_ROOT
from fvttapi.settings.app import FOUNDRY_HOME


class ActorViewSet(viewsets.ViewSet):

    @staticmethod
    def get_actor(world, actor_id):
        db_path = FOUNDRY_HOME / 'Data' / 'worlds' / world / 'data' / 'actors'
        db = plyvel.DB(str(db_path), create_if_missing=False)
        raw_data = [(key, value) for key, value in db]
        db.close()

        values = [json.loads(value) for key, value in raw_data]
        keys = [key.decode('utf8') for key, value in raw_data]

        actor = None
        if actor_id in keys:
            for v in values:
                if v.get('_id') == actor_id:
                    actor = v
                    break
        else:
            for v in values:
                if v.get('name') == actor_id:
                    actor = v
                    break
        return actor

    def list(self, request, *args, world_pk=None, **kwargs):
        db_path = FOUNDRY_HOME / 'Data' / 'worlds' / world_pk / 'data' / 'actors'
        db = plyvel.DB(str(db_path), create_if_missing=False)
        raw_data = [value for key, value in db]
        db.close()

        all_data = [json.loads(value) for value in raw_data]
        data = [{
            '_id': actor['_id'],
            'name': actor.get('name'),
            'img': actor.get('img'),
            'type': actor.get('type'),
        } for actor in all_data]
        page = ListPaginator(request).paginate_list(data)
        return Response(page)

    def retrieve(self, request, *args, world_pk=None, pk=None, **kwargs):
        actor = ActorViewSet.get_actor(world_pk, pk)
        if not actor:
            raise NotFound(f'Actor with that ID/name does not exist in world {world_pk}')
        return Response(actor)

    @action(methods=['get'], detail=True)
    def system(self, request, *args, world_pk=None, pk=None, **kwargs):
        actor = self.get_actor(world_pk, pk)
        return Response(actor['system'])
