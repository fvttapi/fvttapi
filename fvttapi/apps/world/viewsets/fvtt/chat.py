import json
from pathlib import Path

import plyvel
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from django.core.paginator import Paginator

from fvttapi.pagination import ListPaginator
from fvttapi.settings.app import FOUNDRY_HOME
from datetime import datetime

class ChatViewSet(viewsets.ViewSet):
    pagination_class = ListPaginator

    @staticmethod
    def sort_messages(messages):
        return sorted(messages, key=lambda d: d['timestamp'])

    def list(self, request, *args, world_pk=None, **kwargs):
        chat_db = FOUNDRY_HOME / 'Data' / 'worlds' / world_pk / 'data' / 'messages'
        db = plyvel.DB(str(chat_db.resolve()), create_if_missing=False)
        raw_data = [value for key, value in db]
        db.close()

        data = [json.loads(value) for value in raw_data]
        sorted_data = ChatViewSet.sort_messages(data)
        page = ListPaginator(request).paginate_list(sorted_data)
        return Response(page)

    def retrieve(self, request, *args, world_pk=None, pk=None, **kwargs):
        chat_db = FOUNDRY_HOME / 'Data' / 'worlds' / world_pk / 'data' / 'chat.db'
        with open(chat_db, 'r', encoding='utf-8') as file:
            data = file.read().split('\n')
            chats = [json.loads(n) for n in data if len(n) > 2]
            file.close()
        msg = next(iter([m for m in chats if m['_id'] == pk]), None)
        if not msg:
            raise NotFound(f'Message with id {pk} not found in chat log for world {world_pk}.')
        return Response(msg)

    @action(methods=['get'], detail=True)
    def system(self, request, *args, world_pk=None, pk=None, **kwargs):
        chat = self.get_chat(world_pk, pk)
        return Response(chat['system'])
