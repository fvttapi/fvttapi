#from rest_framework import routers
from rest_framework_nested import routers

from .viewsets.fvtt.scene import SceneViewSet
from ..world.viewsets.fvtt.foundry import FoundryViewSet
from ..world.viewsets.fvtt.user import UserViewSet
from ..world.viewsets.fvtt.world import WorldViewSet
from ..world.viewsets.fvtt.actor import ActorViewSet
from ..world.viewsets.fvtt.chat import ChatViewSet

router = routers.SimpleRouter()

router.register(r'^api/world', WorldViewSet, 'worlds')
router.register(r'^api/foundry', FoundryViewSet, 'foundry')

world_router = routers.NestedSimpleRouter(router, r'^api/world', lookup='world')
world_router.register(r'actor', ActorViewSet, basename='world-actors')
world_router.register(r'user', UserViewSet, basename='world-users')
world_router.register(r'chat', ChatViewSet, basename='world-chat')
world_router.register(r'scene', SceneViewSet, basename='world-scene')

url_patterns = [
    *router.urls,
    *world_router.urls,
]
