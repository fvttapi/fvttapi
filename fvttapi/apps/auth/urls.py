from django.urls import re_path
from django.views.generic import TemplateView
from rest_framework_nested import routers

from fvttapi.apps.auth.viewsets import UserViewSet

router = routers.SimpleRouter()

router.register(r'^api/user', UserViewSet, 'user')


url_patterns = [
    *router.urls,
    re_path('^swagger/',
            TemplateView.as_view(
                template_name='swagger.html',
                extra_context={'schema_url': 'openapi-schema'}),
            name='swagger'),

]
