import json

from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import action

from fvttapi.settings.app import FOUNDRY_HOME, FOUNDRY_HOSTNAME
from fvttapi.utils import get_default_world
from ..models import User
from ..serializers import UserSerializer


class UserViewSet(
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        return Response(status=403)

    @action(methods=['get'], detail=False)
    def me(self, request):
        user = request.user
        user_data = self.get_serializer(user, many=False).data

        user_db = FOUNDRY_HOME / 'Data' / 'worlds' / get_default_world() / 'data' / 'users.db'
        with open(user_db, 'r', encoding='utf-8') as file:
            data = file.read().split('\n')
            users = [json.loads(n) for n in data if len(n) > 2]
            file.close()

        player = next(iter([u for u in users if u['name'] == user.username]), None)
        if player:
            return Response({
                **user_data,
                'user': user_data,
                'player': player,
                'world': get_default_world(),
                'game': FOUNDRY_HOSTNAME,
            })
        else:
            return Response(user_data)
