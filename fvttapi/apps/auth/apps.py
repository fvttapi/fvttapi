from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'fvttapi.apps.auth'
    label = 'fvttapi_auth'
