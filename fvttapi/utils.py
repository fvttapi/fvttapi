import json

from fvttapi.settings.app import FOUNDRY_HOME


def get_config():
    options = FOUNDRY_HOME / 'Config' / 'options.json'

    with open(options, 'r', encoding='utf-8') as file:
        config = json.loads(file.read())
        file.close()
    return config


def get_default_world():
    return get_config().get('world', None)
